#!/usr/bin/python3
# Copyright (c) 2020 Bastian Germann
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

"""
Converter for bibleserver_converter's output JSON format.
This program reads one JSON document from stdin and writes one OSIS XML document to stdout.
"""

import json
import sys
import xml.etree.ElementTree as ET

from pysword import canons

osis_books = {90:"Tob", 91:"Jdt", 92:"1Macc", 93:"2Macc", 94:"Wis", 95:"Sir", 96:"Bar"}
canon = canons.canons['kjv']
for i, book in enumerate(canon['ot'] + canon['nt']):
    osis_books[i + 1] = book[1]

def bibleserver_ref_to_osis_ref(canonical):
    canonical = int(canonical)
    osisref = osis_books[canonical // 1000000]
    chapter = (canonical % 1000000) // 1000
    verse = canonical % 1000
    if chapter:
        osisref += f'.{chapter}'
        if verse:
            osisref += f'.{verse}'

    return osisref

def bibleserver_ref_to_natural(booknames, canonical):
    ref = booknames[str(canonical // 1000000)]
    chapter = (canonical % 1000000) // 1000
    verse = canonical % 1000
    # TODO: localize verse separator
    return f"{ref} {chapter},{verse}"

def main():
    doc = json.load(sys.stdin)

    ET.register_namespace('', 'http://www.bibletechnologies.net/2003/OSIS/namespace')
    root = ET.Element('osis')
    root.set('xmlns', 'http://www.bibletechnologies.net/2003/OSIS/namespace')
    root.set('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance")
    root.set('xsi:schemaLocation', "http://www.bibletechnologies.net/2003/OSIS/namespace http://www.bibletechnologies.net/osisCore.2.1.1.xsd")

    osis_text = ET.SubElement(root, 'osisText')
    osis_text.set('osisIDWork', doc['abbreviation'])
    osis_text.set('xml:lang', doc['language']['locale'])

    work = ET.SubElement(ET.SubElement(osis_text, 'header'), 'work')
    work.set('osisWork', doc['abbreviation'])
    title = ET.SubElement(work, 'title')
    title.text = doc['name']
    rights = ET.SubElement(work, 'rights')
    rights.text = doc['copyright']
    ref_system = ET.SubElement(work, 'refSystem')
    ref_system.text = 'Bible'

    div = None
    for i, chapter in doc['chapters'].items():
        chap_id = bibleserver_ref_to_osis_ref(i)
        bookname = chap_id[:-2]

        if chapter['number'] == 1:
            div = ET.SubElement(osis_text, 'div')
            div.set('type', 'book')
            div.set('osisID', bookname)

        chap = ET.SubElement(div, 'chapter')
        chap.set('osisID', chap_id)

        for verse in chapter['verses']:

            if 'heading' in verse:
                heading = ET.SubElement(chap, 'title')
                heading.text = verse['heading']

            text = f"<verse>{verse['content']}"
            if 'footnotes' in verse:
                fmt = text.replace('*', '{}')
                text = fmt.format(*[f"<note>{f['content']}</note>" for f in verse['footnotes']])
            if 'references' in verse:
                text += '<note type="crossReference">'
                for ref in verse['references']:
                    osis_ref = bibleserver_ref_to_osis_ref(ref)
                    nat_ref = bibleserver_ref_to_natural(doc['booknames'], ref)
                    text += f'<reference osisRef="{osis_ref}">{nat_ref}</reference>; '
                text = text[:-2] + '.</note>'
            ver = ET.fromstring(text + '</verse>')
            ver.set('osisID', f"{chap_id}.{verse['verse']}")
            chap.append(ver)

    print(ET.tostring(root, encoding="unicode", xml_declaration=True))

if __name__ == "__main__":
    main()
