#!/usr/bin/python3
# Copyright (c) 2020 Bastian Germann
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

"""
Converter for the internal BibleServer API's JSON format.
Each previous/next chapter click renders two XMLHttpRequest GET requests to
https://www.bibleserver.com/api/cacheable/{BASE64} with {BASE64} being some base64 encoded string.
On the shorter URL's response you can find the presented chapter's JSON data.

This program reads the BibleServer JSON from stdin, expecting each line to be one JSON document
which equals one chapter. It gets rid of empty and redundant data and writes one JSON document containing
all chapters to stdout. Each Bible book that has chapters on the input is checked to be complete.

If you want to write a scraper and need to generate the GET requests automatically,
you will have to decode the {BASE64} to see the plain text as follows:
Decoding {BASE64} with Base64 gives a UTF-8 string with some characters having two bytes (leading 0xC2).
Subtracting 0x15 from each UTF-8 character renders the plain text.
The scraper will need the reverse transformation obviously.
"""

import json
import sys

def chapter_postprocess(chapter, references):
    for verse in chapter['verses']:
        if verse['references']:
            verse['references'] = [ref['canonical'] for ref in verse['references']]
            references.update(verse['references'])
        else:
            del verse['references']

        max_footnotes = verse['content'].count('*')
        for f in verse['footnotes']:
            if f['position'] > max_footnotes:
                print(f"Verse {verse['canonical']}'s footnote {f['position']} does not have an anchor.", file=sys.stderr)
        else:
            del verse['footnotes']

        # errors in MENG
        if verse['canonical'] == 40013045:
            verse['content'] = verse['content'].replace('[BRA ID=0]', '')
        if verse['canonical'] == 12004004:
            verse['content'] = verse['content'].replace('««', '«')
        if verse['canonical'] == 30009001:
            verse['content'] = verse['content'].replace('»»', '»')
        if verse['canonical'] == 43018004:
            verse['content'] = verse['content'].replace('»»»', '»')

def main():
    readchapters = {}
    references = set()
    doc = None
    for line in sys.stdin:
        if not line.strip():
            continue
        data = next(iter(json.loads(line)['data'].values()))
        bible = data['chapter'].pop('bible')
        if not doc:
            doc = bible
            doc['booknames'] = {}
            doc['chapters'] = {}
        elif doc['id'] != bible['id']:
            print(f"A chapter from {bible['name']} does not fit into {doc['name']}", file=sys.stderr)
            sys.exit(1)

        chapter = data['chapter']
        chapter_postprocess(chapter, references)
        canonical = chapter.pop('canonical')
        doc['chapters'][canonical] = chapter
        doc['booknames'].update(data['booknames'])

        k = canonical // 1000000
        if k in readchapters:
            readchapters[k][0] += 1
        else:
            readchapters[k] = [1,0]
        if not data['next']:
            readchapters[k][1] = chapter['number']

    if doc:
        json.dump(doc, sys.stdout, separators=(',', ':'), sort_keys=True)

    for k, v in readchapters.items():
        if v[0] != v[1]:
            print(f"Read {v[0]} instead of {v[1]} chapters for book {doc['booknames'][str(k)]}.", file=sys.stderr)

    checked_references = set()
    for k in references:
        for verse in doc['chapters'][(k//1000)*1000]['verses']:
            if verse['canonical'] == k:
                checked_references.add(k)
    if references != checked_references:
        print(f"Could not find canonical references {references - checked_references} in the given chapters.", file=sys.stderr)

if __name__ == "__main__":
    main()
