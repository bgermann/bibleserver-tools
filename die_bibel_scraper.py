#!/usr/bin/python3
# Copyright (c) 2021 Bastian Germann
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

"""
Scraper for the internal die-bibel.de API's JSON format. Scrolling an online Bible on die-bibel.de
renders XMLHttpRequest GET requests to https://ibep.ubscloud.org/bibles/{ID}/chapters/{CHAPTER}
with {ID} being a unique identifier for the Bible translation and {CHAPTER} being a three-letter
book identifier, followed by a dot, followed by the chapter number or "intro", e.g., GEN.1 or
GEN.intro. The response has a chapter's JSON data.

This program takes bible translation IDs as command line parameters.
Each Bible is downloaded and the JSON data written to stdout, each line being a chapter.
An empty line marks the end of a Bible.
"""

import json
import sys
import urllib3

def download(bible_id, chapter_id):
    "Downloads one bible chapter, saves it with name chapter_id, and returns the next chapter_id."
    http = urllib3.PoolManager()
    r = http.request('GET', f'https://ibep.ubscloud.org/bibles/{bible_id}/chapters/{chapter_id}',
                     fields={'content-type':'json', 'include-chapter-numbers':'true', 'include-verse-numbers':'true', 'include-notes':'true', 'include-titles':'true'},
                     headers={'user-token':'anonymous', 'brand-id':'DIEBIBEL', 'x-api-key':'896f6f87-fc95-4605-b782-804b99b83800'})
    if r.status != 200:
        print(f"{bible_id}'s chapter {chapter_id} failed to download.", file=sys.stderr)
        sys.exit(1)
    data = r.data.decode('utf-8')
    doc = json.loads(data)
    print(data)
    if 'next' in doc['data']:
        return doc['data']['next']['id']
    return None

def main():
    for bible_id in sys.argv[1:]:
        chapter_id = 'GEN.intro'
        while chapter_id:
            chapter_id = download(bible_id, chapter_id)
        print()

if __name__ == "__main__":
    main()
